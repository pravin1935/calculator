import React from 'react';
import logo from './logo.svg';
import './App.css';

export default class ResultComponent extends React.Component() {
  
  constructor(){
    super();
  }

  render(){

    return(
      <div>
          <p>{this.props.result}</p>
      </div>
      );
  }
}

