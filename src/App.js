import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import ResultComponent from './components/ResultComponent';
import KeyboardComponent from './components/KeyboardComponent';
import ErrorBoundary from './components/ErrorBoundary';

export default class App extends Component {

    constructor(props) {
        super();
        this.state = {
            result: '',
            start: 'false' , 
            'haserror': false
        };

        this.onclick = this.onclick.bind(this);
    }

   

    onclick(event) {
        var name = event.target.name;
        var result_final = '';
        if (name === 'total') {
            result_final = this.state.result;

            result_final = result_final.replace('--', '+');
            // console.log(eval(result_final));
            result_final = result_final.replace(/^0+/ , '');
            // here work is to multiple zero in any form before expression
            console.log(result_final);
            result_final = result_final.replace('**', '*');
            result_final = result_final.replace('++', '+');
            result_final = result_final.replace('//', '/');

            try{
            this.setState({ result: (eval(result_final) || "") });
            }
            catch(e){
                this.setState({result:'error'});
            }
            
            this.setState({ start: 'true' });                

        } else if (name === 'bs') {

            this.setState({ result: this.state.result.slice(0, -1) });
        } else if (name === 'clr') {

            this.setState({ result: "" });
        } else {


            var result1 = this.state.result + event.target.name;
            if (this.state.start === 'true') {

                this.setState({ 'result': event.target.name });
                this.setState({ 'start': 'false' });

            } else {

                this.setState({ result: result1 });

            }


        }

    }

    render() {


        if(this.state.haserror){
            return(
                this.setState({result: "error occured"})
                )
        }
        return (
            <div>
           <div className="container">
           <div className="row justify-content-md-center">
            
            <div className="calc_wrapper">
            <div className="">
             <div className="other-text">
              <h2>Free Online Calculators</h2>
              <p>This calculator is designed in bootstrap and developed in react by Nepali Developer Pravin</p>
              <hr />
              <h1>PRAVIN POUDEL</h1>
              <p>Build on : React JS </p>
            </div>
            </div>

            <div className="">
           
            <div className ="calculatorbody">
            <div className="result-wrapper">
              
                    <ResultComponent result={this.state.result} />
              
            </div>
            
              <KeyboardComponent onclick = {this.onclick} />
            
            </div>
            
            </div>
      </div>
      </div>
            </div>
      </div>



        );
    }
}