import React, { Component } from 'react';

export default class ResultComponent extends Component {

    render() {

        return (
            <div className= "key_wrapper">

		    <button name="clr" onClick = {(event)=> {this.props.onclick(event)}}> CE </button>      		
		    <button name="*" onClick = {(event)=> {this.props.onclick(event)}}> * </button>
		    <button name="bs" onClick = {(event)=> {this.props.onclick(event)}}> C </button>


      		<button name="1" onClick = {(event)=> {this.props.onclick(event)}}> 1 </button>
      		<button name="2" onClick = {(event)=> {this.props.onclick(event)}}> 2 </button>
      		<button name="3" onClick = {(event)=> {this.props.onclick(event)}}> 3 </button>
      		
      		<button name="4" onClick = {(event)=> {this.props.onclick(event)}}> 4 </button>
      		<button name="5" onClick = {(event)=> {this.props.onclick(event)}}> 5 </button>
      		<button name="6" onClick = {(event)=> {this.props.onclick(event)}}> 6 </button>
      		<button name="7" onClick = {(event)=> {this.props.onclick(event)}}> 7 </button>
      		
      		<button name="8" onClick = {(event)=> {this.props.onclick(event)}}> 8 </button>
      		<button name="9" onClick = {(event)=> {this.props.onclick(event)}}> 9 </button>
			<button name="+" onClick = {(event)=> {this.props.onclick(event)}}> + </button>
      		
      		<button name="0" onClick = {(event)=> {this.props.onclick(event)}}> 0 </button>
      		<button name="-" onClick = {(event)=> {this.props.onclick(event)}}>-</button>
      		<button name="/" onClick = {(event)=> {this.props.onclick(event)}}> % </button>
	      	<button name="total" onClick = {(event)=> {this.props.onclick(event)}}> = </button>
      		<button name="." onClick = {(event)=> {this.props.onclick(event)}}> . </button>
      	

	      </div>
        );
    }

}